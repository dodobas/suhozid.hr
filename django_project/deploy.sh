#!/bin/bash

VENV=/home/dodobas/suhozid_pyenv/bin
while :
do
    date
    echo "Waiting for changes...\n\n"
    inotifywait -r -e close_write --exclude '(mediafiles|staticfiles|wsgi.py|pyc)' .

    echo "Deploying service...\n\n"
    $VENV/python manage.py collectstatic --noinput
    touch core/wsgi.py

done
