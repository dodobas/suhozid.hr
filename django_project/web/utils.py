from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.conf import settings


def CachePage(theTimeout=None, *args, **kwargs):
    """
    Apply the ``cache_page`` decorator to all the handlers in a class-based
    view that delegate to the ``dispatch`` method.

    Examples:
        @CachePage(timeout=60 * 10)
        class MyView(TemplateView):

        # we must 'CALL' the decorator as it needs to return a class
        # if we don't specify a timeout, settings.CACHE_MIDDLEWARE_SECONDS will
        # be used
        @CachePage()
        class MyView(TemplateView):
    """

    # Check that the View class is a class-based view by checking that the
    # ViewClass has a ``dispatch`` method.
    def f(cls):
        if not hasattr(cls, 'dispatch'):
            raise TypeError((
                'View class is not valid: %r.  Class-based views '
                'must have a dispatch method.') % cls)

        original = cls.dispatch

        if theTimeout:
            myTimeout = theTimeout
        else:
            myTimeout = settings.CACHE_MIDDLEWARE_SECONDS

        modified = method_decorator(cache_page(myTimeout, **kwargs))(original)

        cls.dispatch = modified
        return cls

    return f
