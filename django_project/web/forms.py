import django.forms as forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Layout,
    Submit,
    Div,
    Field
)
from crispy_forms.bootstrap import (
    FormActions,
)

from django.contrib.auth.models import User


class UserForm(forms.ModelForm):
    class Meta:
        model = User

        fields = ['username', 'email']

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.help_text_inline = True
        self.helper.html5_required = False

        self.helper.layout = Layout(
            Div(Field('username')),
            Div(Field('email')),
            FormActions(
                Submit('save', 'Spremi promjene', css_class="btn-success"),
            )
        )

        super(UserForm, self).__init__(*args, **kwargs)
