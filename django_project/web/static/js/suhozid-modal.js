!function ($) {

  var SuhozidModal = function (element, options) {
    this.options = options;
    this.element = $(element);
    this.init();
  };


  SuhozidModal.prototype = {
    constructor: SuhozidModal,

    get_type: function (type) {
        var myType = parseInt(type,10);
        switch (myType){
            case 1: return 'Zid';
            case 2: return 'Građevina';
            case 3: return 'Naselje';
            case 4: return 'Krajolik';
            case 5: return 'Tematska staza';
            default: return 'Nepoznato obilježje';
        }
    },

    init: function() {
        var self=this;

        this.element.html(JST.lokalitet_info({}));

        this.mydialog = this.element.modal({
            show: false,
            modal: true
        });

        this.title = this.mydialog.find('.modal-title');
        this.body = this.mydialog.find('.modal-body');
        this.buttons = this.mydialog.find('.modal-footer');

        this._prepare_data();
        this.mydialog.modal('show');

        // remove modal when hidden
        this.mydialog.on('hidden', function (evt) {
            self._destroy();
        })
    },

    _prepare_data: function () {
        var self=this;
        this.body.html(
            JST.feature_modal({
                'features':this.options.features,
                'self':self,
                //Send hostname with port, not full current URL as with document.URL
                'url': window.location.host
            }));
    },
    _destroy: function () {
        this.element.remove();
    }
  };

  $.fn.suhozidmodal = function ( option ) {
    return this.each(function () {
      var $this = $(this), data = $this.data('SuhozidModal'), options = typeof option == 'object' && option;
      if (!data) $this.data('SuhozidModal', (data = new SuhozidModal(this, options)));
      if (typeof option == 'string') data[option]();
    });
  };

}(window.jQuery);
