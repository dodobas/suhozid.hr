
// instantiate new object
var theMap = new APP.OLMap('map');

function toggleShareModal(modalId) {
	var mymodal = $('#' + modalId);
    mymodal.modal('show');

    mymodal.on('hidden', function(evt) {
        // don't propagate the event
        return false;
    })
}

function hideShareModal(modalId) {
    var mymodal = $('#' + modalId);
    mymodal.modal('hide');
}

//Style scrollbar when document is ready.
$(function() {
    $('#right_sidebar').jScrollPane();
});
