!function(){

APP.GFIControl = OpenLayers.Class(OpenLayers.Control, {
            defaultHandlerOptions: {
                'single': true,
                'double': false,
                'pixelTolerance': 0,
                'stopSingle': false,
                'stopDouble': false
            },

            initialize: function(options) {
                this.handlerOptions = OpenLayers.Util.extend({}, this.defaultHandlerOptions);
                OpenLayers.Control.prototype.initialize.apply(
                this, arguments);
                this.handler = new OpenLayers.Handler.Click(
                this, {
                    'click': this.onClick,
                    'dblclick': this.onDblclick
                }, this.handlerOptions);
            },

            onClick: function(evt) {
                var bbox = this.map.getExtent().toBBOX(null);
                var height = this.map.getSize().h;
                var width = this.map.getSize().w;
                var clickX = evt.xy.x;
                var clickY = evt.xy.y;

                $.ajax({
                    url:'/getfeatureinfo/',
                    data: {'bbox': bbox, 'height': height, 'width': width, 'clickX': clickX, 'clickY': clickY},
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length > 0) {
                        $('<div>').suhozidmodal({'features':data});
                    }
                });
            }
    });

//Return 10 closest Localities when you hover over one.
//Return number of localities defined in -> suhozid.views.GetLocalityInfo
APP.GFIHoverControl = OpenLayers.Class(OpenLayers.Control, {
             //Settings for hover handler.
             defaultHandlerOptions: {
                delay: 500,
                //When popup is created, give user some room to go into popup.
                pixelTolerance: 5,
                stopMove: false
            },

            //Init object.
            initialize: function(options) {
                this.handlerOptions = OpenLayers.Util.extend({}, this.defaultHandlerOptions);
                OpenLayers.Control.prototype.initialize.apply(
                    this, arguments);
                this.handler = new OpenLayers.Handler.Hover(
                    this, {
                        'pause': this.onPause,
                        'move': this.onMove
                    }, this.handlerOptions);
            },

            //List containing active popups.
            popupList: new Array(),

            //On mouse pause show popup with ID of location.
            onPause: function(evt) {
                var bbox = this.map.getExtent().toBBOX(null);
                var height = this.map.getSize().h;
                var width = this.map.getSize().w;
                var clickX = evt.xy.x;
                var clickY = evt.xy.y;

                //Store "this" reference of object.
                var self = this;

                //Do an ajax request for data.
                $.ajax({
                    url:'/getfeatureinfo/',
                    data: {'bbox': bbox, 'height': height, 'width': width, 'clickX': clickX, 'clickY': clickY},
                    dataType: 'json'
                }).done(function (data) {
                    if (data.length > 0) {
                        //Remove existing popup before creating new one then empty the array.
                        if (self.popupList.length > 0) {
                            self.map.removePopup(self.popupList[0]);
                            self.popupList.length = 0;
                        }

                        //Array holding strings to show on hover.
                        var locationInfo = [];

                        //String holding current name of location
                        var locationTitle = '';

                        //Fill "locationInfo" with HTML string in format:
                        //<a class="popupText" href="/#show/ID">LocationName</a><br>
                        //Purpose of class "popupText" is to id content in
                        //popup, so when we check if we need to close popup
                        //we know user if user is hovering over the popup content
                        _.each(data, function(location) {
                            if (location.title.length >= 25) {
                                locationTitle = location.title.substr(0,22);
                                locationTitle = locationTitle + '...';
                            }
                            else {
                                locationTitle = location.title;
                            }
                            constructedHTML = '<a class="popupText" href="/#show/' + location.id.toString() + '">' + locationTitle + '</a><br>';
                            locationInfo.push(constructedHTML);
                        });

                        //Figure out X-size needed for popup.
                        //Minimum size of 20 for chrome, if less popup is unusable.
                        var boxSizeX = 180;
                        var boxSizeY = 20 * data.length;

                        //Find out longitude and latitude where popup is to be set.
                        currentMousePositionPixel = new OpenLayers.Pixel(clickX, clickY);
                        popupLonLat = self.map.getLonLatFromViewPortPx(currentMousePositionPixel);

                        //Create new popup.
                        popup = new OpenLayers.Popup.Anchored(
                            null,
                            popupLonLat,
                            new OpenLayers.Size(boxSizeX, boxSizeY),
                            locationInfo.join('\n'),
                            null,
                            false
                            );

                        //Store it in popup list and add it to map.
                        self.popupList.push(popup);
                        theMap.map.addPopup(popup);
                    }
                });
            },

            //Remove popup if user moves outside the popup.
            onMove: function(event) {
                //Don't execute the function if there is no popup.
                if (this.popupList.length > 0) {
                    //Check if user hovers over popup, if so don't close the popup.
                    //Every popup has class "olPopupContent" and "olPopup",
                    //text inside popup has class "popupText".
                    if(event.target.className === 'olPopup' ||
                        event.target.className === 'olPopupContent' ||
                        event.target.className ===  'popupText') {
                        return;
                    }

                    //Remove popup from map then empty the array.
                    this.map.removePopup(this.popupList[0]);
                    this.popupList.length = 0;
            }
        }
    });

APP.OLMap = function (element, options) {
    this.options = options;
    this.$element = $(element);
    this.element = element;
    var self = this;

    //initialize map
    this.initMap();
};


APP.OLMap.prototype = {
    //constructor: APP.OLMap,

    gsat: new OpenLayers.Layer.Google(
        "Google Satellite", {
            type: google.maps.MapTypeId.SATELLITE,
            numZoomLevels: 22
        }
    ),

    suhozidLayer: new OpenLayers.Layer.WMS(
        "Suhozid",
        "http://suhozid.geof.unizg.hr/mapserv", {
            layers: "suhozid_2013_fanatic",
            transparent:'true'
        },{
            singleTile: 'true'
        }
    ),

    suhozidWebLayer: new OpenLayers.Layer.WMS(
        "Suhozid Web",
        "http://suhozid.geof.unizg.hr/mapserv", {
            layers: "suhozid_webpoint",
            transparent:'true'
        },{
            singleTile: 'true'
        }
    ),

    suhozidODKLayer: new OpenLayers.Layer.WMS(
        "Suhozid ODK",
        "http://suhozid.geof.unizg.hr/mapserv", {
            layers: "suhozid_odkeasypoint,suhozid_odkpoint",
            transparent:'true'
        },{
            singleTile: 'true'
        }
    ),

    webPointLayer: new OpenLayers.Layer.Vector("webPointLayer", {
        displayInLayerSwitcher: false
    }),

    tmpPointLayer: new OpenLayers.Layer.Vector("tmpPointLayer", {
        displayInLayerSwitcher: false
    }),

    createMap: function () {
        return new OpenLayers.Map({
            div: this.element,
            controls: [
                new OpenLayers.Control.Navigation(),
                new OpenLayers.Control.PanZoom(),
                new OpenLayers.Control.Attribution(),
                // new OpenLayers.Control.LayerSwitcher(),
                new OpenLayers.Control.Permalink('share-view', 'http://suhozid.geof.unizg.hr/')
            ],
            projection: new OpenLayers.Projection("EPSG:900913"),
            units: "m",
            maxResolution: 156543.0339,
            maxExtent: new OpenLayers.Bounds(
                -20037508.34, -20037508.34, 20037508.34, 20037508.34),
            restrictedExtent: new OpenLayers.Bounds(1277456,5205290,2463229,5921863)
        });
    },

    osmLayer: function () {
        return new OpenLayers.Layer.OSM(
            'OpenStreetMap', null, {
                attribution: "&copy; <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> contributors"
            });
    },

    initMap: function() {
        var self = this;

        this.map = this.createMap();

        this.map.addLayer(this.osmLayer());

        this.map.addLayer(this.gsat);

        this.map.addLayer(this.suhozidLayer);
        this.map.addLayer(this.suhozidODKLayer);
        this.map.addLayer(this.suhozidWebLayer);

        this.info = new APP.GFIControl();
        this.map.addControl(this.info);
        this.info.activate();

        this.infoHover = new APP.GFIHoverControl();
        this.map.addControl(this.infoHover);
        this.infoHover.activate();

        this.drawFeature = new OpenLayers.Control.DrawFeature(
            this.webPointLayer,
            OpenLayers.Handler.Point
        );
        this.map.addControl(this.drawFeature);

        this.drawFeature.layer.events.on({
            featureadded: $.proxy(this.featureadded, null, self)
        });

        this.modifyFeature = new OpenLayers.Control.ModifyFeature(
            this.webPointLayer, {
                standalone: true
            });

        this.map.addControl(this.modifyFeature);

        this.map.addLayer(this.webPointLayer);
        this.map.addLayer(this.tmpPointLayer);

        this._update_layer_picker();

        this.setupEvents();

        //zoom to max extent
        if (!this.map.getCenter()) {
            this.map.zoomToMaxExtent();
        }

    },

    _update_layer_picker:function() {
        var myPickableLayers = _.filter(this.map.layers, function(layer) {
            return layer.displayInLayerSwitcher===true;
        });
        var myBaseLayers = _.filter(myPickableLayers, function(layer) {
            return layer.isBaseLayer===true;
        });
        var myOverlayLayers = _.filter(myPickableLayers, function(layer) {
            return layer.isBaseLayer===false;
        });
        $('#layer_picker').html(JST.layer_picker({
            'baseLayers': myBaseLayers,
            'overlayLayers': myOverlayLayers
        })
        );
    },

    featureadded: function (self, event) {
        self.drawFeature.deactivate();
        self.modifyFeature.activate();
        self.modifyFeature.selectFeature(self.webPointLayer.features[0]);
        $('#save-point').toggleClass('hide');
    },

    setupEvents: function () {
        var self = this;

        // on layerpicker click
        $('#layer_picker').on('click', 'a', function(evt){
            // get the first layer
            var myTargetLayer = self.map.getLayersByName(evt.target.text)[0];

            if (myTargetLayer.isBaseLayer) {
                self.map.setBaseLayer(myTargetLayer, false);
            } else {
                if (myTargetLayer.visibility) {
                    myTargetLayer.setVisibility(false);
                } else {
                    myTargetLayer.setVisibility(true);
                }
            }
            self._update_layer_picker();

            evt.preventDefault();
        });
        // on search on map
        $('#search-on-map').click(function (evt) {
            evt.preventDefault();
            if (!self.info.active) {
                self.info.activate();
                self.drawFeature.deactivate();
                self.modifyFeature.deactivate();
                $(this).toggleClass('active');
                $('#add-new-point').toggleClass('active');
                $('#save-point').addClass('hide');
                self.webPointLayer.removeAllFeatures();
                self.tmpPointLayer.removeAllFeatures();
            }
        });
        // on add new point
        $('#add-new-point').click(function (evt) {
            evt.preventDefault();
            self.info.deactivate();
            self.drawFeature.activate();
            $(this).toggleClass('active');
            $('#search-on-map').toggleClass('active');
            self.tmpPointLayer.removeAllFeatures();
        });
        // on click save point
        $('#save-point').click(function (evt) {
            var myFeature = self.webPointLayer.features[0];
            var wktFormat = new OpenLayers.Format.WKT({
                'internalProjection': self.map.getProjectionObject(),
                'externalProjection': new OpenLayers.Projection("EPSG:4326")
            });
            $('<div>').ce_modal({
                'url':'/createwebpoint/',
                'form_values': {
                    'point': wktFormat.write(myFeature)
                }
            });
        });

        // on point saved
        $APP.bind('point-saved', function (evt) {
            self.drawFeature.activate();
            self.modifyFeature.deactivate();
            $('#save-point').addClass('hide');
            self.webPointLayer.removeAllFeatures();

            // redraw layer, we neet to add new request parameters
            self.suhozidWebLayer.mergeNewParams({"_olSalt": Math.random()});
            self.suhozidWebLayer.redraw();
        });

        //add routes
        $.routes.add("show/{id:int}", function () {
            // ajax get point data
            $.ajax({
                url:'/getlocalityinfo/',
                data: {'pk': this.id},
                dataType: 'json'
            }).done(function (data) {
                var myPoint = new OpenLayers.Format.WKT({
                    'internalProjection': new OpenLayers.Projection("EPSG:900913"),
                    'externalProjection': new OpenLayers.Projection("EPSG:4326")
                }).read(data['coordinate_raw']);

                //show modal window
                $('<div>').suhozidmodal({'features':[data]});

                //remove all features
                self.tmpPointLayer.removeAllFeatures();
                self.tmpPointLayer.addFeatures([myPoint]);
                self.map.setCenter(new OpenLayers.LonLat(myPoint.geometry.x,myPoint.geometry.y), 15);
        });
    });
    }
}; //prototype

}();