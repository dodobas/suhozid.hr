$(function() {
	//Select full URL from "Prikaži na karti" button.
	var shareUrl = $('#show-on-map').prop('href');
	//Display URL in input element when users clicks "Share" button.
	$('#social-url').attr('value', shareUrl);

	//Get host and ID of current locality. Then construct links for social networks.
	var host = window.location.host;
	var shareID = $('.modal-body').attr('id');

	//Twitter
	var twitterURL = 'https://twitter.com/share?text=Zanimljiv suhozid&url=http%3A%2F%2F'
	+ host + '%2Fdetail%2F'+  shareID;
	$('.twitter').attr('href', twitterURL);

	//Facebook
	var facebookURL = 'https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F' + host + '%2Fdetail%2F' + shareID;
	$('.facebook').attr('href', facebookURL);

	//Google+
	var googleURL = 'https://plus.google.com/share?url=http%3A%2F%2F' + host + '%2Fdetail%2F' + shareID;
	$('.google').attr('href', googleURL);
});