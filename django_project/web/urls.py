from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

from .views import (
    HomeView,
    AboutProjectView,
    ContactView,
    LogoutUser,
    LoginPage,
    ProfilePage
)

urlpatterns = patterns(
    '',
    url(r'^$', HomeView.as_view(), name='homeview'),
    url(
        r'^o_projektu/$',
        AboutProjectView.as_view(), name='aboutview'),
    url(
        r'^kontakt/$',
        ContactView.as_view(), name='contactview'),
    url(
        r'^odjava$',
        LogoutUser.as_view(), name='logout_user'
    ),
    url(
        r'^prijava$',
        LoginPage.as_view(), name='login_user'
    ),
    url(
        r'^uredi_profil$',
        ProfilePage.as_view(), name='profile_page'
    )
)

# if we are in DEBUG mode, django should serve static files
if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += patterns(
        '', url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.STATIC_ROOT})
    )
