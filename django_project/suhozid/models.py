# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django.db.models import Q
from django.core.urlresolvers import reverse


from model_utils.models import TimeStampedModel
from model_utils import Choices
import reversion

from .geoInheritanceManager import GeoInheritanceManager
from .utils import format_coordinate, format_datetime
from .markitup_fields import MarkupFieldRaw


class LocalityManager(GeoInheritanceManager):
    def filter_by_location(self, *args, **kwargs):
        qs = Q()
        for subclass in self.model.__subclasses__():
            qs |= subclass.filter_by_location(*args, **kwargs)

        return super(LocalityManager, self).get_query_set().filter(qs)


class Locality(TimeStampedModel):
    STATUS = Choices(
        (0, 'deleted', 'Deleted'), (1, 'hidden', 'Hidden'),
        (2, 'in_review', 'In review'), (3, 'published', 'Published'))

    locality_title = models.CharField(max_length=200)
    author = models.ForeignKey('auth.User', null=True, blank=True)
    short_text = MarkupFieldRaw(blank=True)
    main_text = MarkupFieldRaw(blank=True)
    status = models.IntegerField(choices=STATUS, default=STATUS.hidden)

    objects = LocalityManager()

    def get_absolute_url(self):
        return reverse('localitydetail', args=[self.pk])

    def get_author(self):
        if self.author:
            return str(self.author)
        else:
            return None

# register model with django-reversion
reversion.register(Locality)


class WebPoint(Locality):
    anonymous_author = models.CharField(
        max_length=60, null=True, blank=True,
        verbose_name=u'Autor'
    )
    description = models.TextField(
        null=True, blank=True,
        verbose_name=u'Opis/Komentar'
    )
    image_name = models.CharField(
        max_length=100, verbose_name=u'Naziv fotografije'
    )
    image = models.ImageField(
        upload_to='user_images', verbose_name=u'Fotografija',
        help_text=(
            u'Fotografije zahtjevaju dodatnu obradu stoga treba pričekati 10 '
            'minuta kako bi postale vidljive na portalu!'
        )
    )
    point = models.PointField()
    collection_timestamp = models.DateTimeField(auto_now_add=True)

    objects = models.GeoManager()

    LOCTYPE = 'Web'

    @classmethod
    def filter_by_location(cls, *args, **kwargs):
        return Q(**{
            '{0}__point__within'.format(cls._meta.module_name): args[0]
        })

    def get_short_text(self):
        return self.short_text.rendered if self.short_text else None

    def get_main_text(self):
        return self.main_text.rendered if self.main_text else None

    def get_short_info(self):
        return {
            'id': self.id,
            'title': self.image_name,
            'images': [self.image.name],
            'short_text': self.get_short_text() or self.description,
            'author': (
                self.get_author() or self.anonymous_author or 'Nije poznat'
            ),
            'coordinate': format_coordinate(self.point),
            'coordinate_raw': self.point.wkt,
            'created': format_datetime(self.created),
            'source': self.LOCTYPE
        }

    def get_detailed_info(self):
        return {
            'id': self.id,
            'title': self.locality_title or self.image_name,
            'images': [self.image.name],
            'main_text': self.get_main_text() or self.description,
            'author': (
                self.get_author() or self.anonymous_author or 'Nije poznat'
            ),
            'coordinate': format_coordinate(self.point),
            'created': format_datetime(self.created),
            'source': self.LOCTYPE
        }

    def get_original_data(self):
        return [
            ('id', self.id),
            ('description', self.description),
            ('image_name', self.image_name),
            ('anonymous_author', self.anonymous_author)
        ]

    def get_object_info(self):
        return {
            'id': self.id,
            'type': self.LOCTYPE,
            'name': self.locality_title or self.image_name or 'Bez naziva'
        }


class ODKFanaticData(models.Model):
    anonymous_author = models.CharField(
        max_length=60, null=True, blank=True,
        verbose_name=u'Autor'
    )
    localitytype = models.IntegerField(verbose_name=u'Vrsta lokaliteta')
    name = models.CharField(
        max_length=100, null=True, blank=True, verbose_name=u'Naziv')
    remark = models.TextField(null=True, blank=True, verbose_name=u'Napomena')
    outer_width = models.FloatField(
        null=True, blank=True, verbose_name=u'Vanjska širina')
    outer_length = models.FloatField(
        null=True, blank=True, verbose_name=u'Vanjska dužina')
    outer_height = models.FloatField(
        null=True, blank=True, verbose_name=u'Vanjska visina')
    inner_width = models.FloatField(
        null=True, blank=True, verbose_name=u'Unutarnja širina')
    inner_length = models.FloatField(
        null=True, blank=True, verbose_name=u'Unutarnja dužina')
    inner_height = models.FloatField(
        null=True, blank=True, verbose_name=u'Unutarnja visina')
    entrance_height = models.IntegerField(
        null=True, blank=True, verbose_name=u'Visina ulaza')
    entrance_width = models.IntegerField(
        null=True, blank=True, verbose_name=u'Širina ulaza')
    entrance_orientation = models.IntegerField(
        null=True, blank=True, verbose_name=u'Orijentacija')
    guide = models.CharField(
        max_length=150, null=True, blank=True, verbose_name=u'Vodić')
    extra_photos = models.CharField(max_length=250, null=True, blank=True)
    collection_timestamp = models.DateTimeField()
    device_id = models.CharField(max_length=50)
    observation_uuid = models.CharField(max_length=50)


class ODKFanaticPoint(Locality):
    image = models.ImageField(
        upload_to='odk_images', verbose_name=u'Fotografija')
    point = models.PointField()

    height = models.FloatField(blank=True, null=True)
    accuracy = models.FloatField(blank=True, null=True)

    odkdata = models.ForeignKey('ODKFanaticData')

    objects = models.GeoManager()

    LOCTYPE = 'ODKFanatic'

    @classmethod
    def filter_by_location(cls, *args, **kwargs):
        return Q(**{
            '{0}__point__within'.format(cls._meta.module_name): args[0]
        })

    def prepare_images(self):
        if self.odkdata.extra_photos:
            return [self.image.name] + [
                'dodatne/{0}'.format(image)
                for image in self.odkdata.extra_photos.split('**') if image]
        else:
            return [self.image.name]

    def get_short_text(self):
        return self.short_text.rendered if self.short_text else None

    def get_main_text(self):
        return self.main_text.rendered if self.main_text else None

    def get_short_info(self):
        return {
            'id': self.id,
            'title': self.odkdata.name or 'Bez naziva',
            'images': self.prepare_images(),
            'short_text': self.get_short_text() or self.odkdata.remark,
            'author': self.odkdata.anonymous_author or 'Nije poznat',
            'coordinate': format_coordinate(self.point),
            'coordinate_raw': self.point.wkt,
            'created': format_datetime(self.created),
            'source': self.LOCTYPE
        }

    def get_detailed_info(self):
        return {
            'id': self.id,
            'title': self.locality_title or self.odkdata.name or 'Bez naziva',
            'images': self.prepare_images(),
            'main_text': self.get_main_text() or self.odkdata.remark,
            'author': self.odkdata.anonymous_author or 'Nije poznat',
            'coordinate': format_coordinate(self.point),
            'created': format_datetime(self.created),
            'source': self.LOCTYPE
        }

    def get_original_data(self):
        return [
            ('id', self.id),
            ('localitytype', self.odkdata.localitytype),
            ('name', self.odkdata.name),
            ('remark', self.odkdata.remark),
            ('outer_width', self.odkdata.outer_width),
            ('outer_length', self.odkdata.outer_length),
            ('outer_height', self.odkdata.outer_height),
            ('inner_width', self.odkdata.inner_width),
            ('inner_length', self.odkdata.inner_length),
            ('inner_height', self.odkdata.inner_height),
            ('entrance_height', self.odkdata.entrance_height),
            ('entrance_width', self.odkdata.entrance_width),
            ('entrance_orientation', self.odkdata.entrance_orientation),
            ('guide', self.odkdata.guide),
            ('extra_photos', self.odkdata.extra_photos),
            ('collection_timestamp', self.odkdata.collection_timestamp),
            ('device_id', self.odkdata.device_id),
            ('observation_uuid', self.odkdata.observation_uuid),
        ]

    def get_object_info(self):
        return {
            'id': self.id,
            'type': self.LOCTYPE,
            'name': self.locality_title or self.odkdata.name or 'Bez naziva'
        }


class Suhozid2013Fanatic(Locality):
    wkb_geometry = models.PointField(null=True, blank=True)
    naziv = models.CharField(max_length=100, blank=True)
    vrsta_lok = models.IntegerField(null=True, blank=True)
    opis_nap = models.TextField(blank=True)
    vanj_vis = models.CharField(max_length=20, blank=True)
    vanj_sir = models.CharField(max_length=20, blank=True)
    vanj_dulj = models.CharField(max_length=20, blank=True)
    unu_vis = models.CharField(max_length=20, blank=True)
    unu_sir = models.CharField(max_length=20, blank=True)
    unu_dulj = models.CharField(max_length=20, blank=True)
    ulaz_vis = models.CharField(max_length=20, blank=True)
    ulaz_sir = models.CharField(max_length=20, blank=True)
    ulaz_ori = models.IntegerField(null=True, blank=True)
    vodic = models.CharField(max_length=20, blank=True)
    autor_unosa = models.CharField(max_length=20, blank=True)
    foto = models.CharField(max_length=20, blank=True)

    LOCTYPE = 'GIS'

    objects = models.GeoManager()

    class Meta:
        db_table = 'suhozid_2013_fanatic'

    @classmethod
    def filter_by_location(cls, *args, **kwargs):
        return Q(**{
            '{0}__wkb_geometry__within'.format(cls._meta.module_name): args[0]
        })

    def prepare_images(self):
        if self.foto:
            return [
                'fotke/{0}'.format(image)
                for image in self.foto.split('**') if image]
        else:
            return None

    def get_short_text(self):
        return self.short_text.rendered if self.short_text else None

    def get_main_text(self):
        return self.main_text.rendered if self.main_text else None

    def get_short_info(self):
        return {
            'id': self.id,
            'title': self.naziv or 'Bez naziva',
            'images': self.prepare_images(),
            'short_text': self.get_short_text() or self.opis_nap,
            'author': self.autor_unosa or 'Nije poznat',
            'coordinate': format_coordinate(self.wkb_geometry),
            'coordinate_raw': self.wkb_geometry.wkt,
            'created': format_datetime(self.created),
            'source': self.LOCTYPE
        }

    def get_detailed_info(self):
        return {
            'id': self.id,
            'title': self.locality_title or self.naziv or 'Bez naziva',
            'images': self.prepare_images(),
            'main_text': self.get_main_text() or self.opis_nap,
            'author': self.autor_unosa or 'Nije poznat',
            'coordinate': format_coordinate(self.wkb_geometry),
            'created': format_datetime(self.created),
            'source': self.LOCTYPE
        }

    def get_original_data(self):
        return [
            ('id', self.id),
            ('naziv', self.naziv),
            ('vrsta_lok', self.vrsta_lok),
            ('opis_nap', self.opis_nap),
            ('vanj_vis', self.vanj_vis),
            ('vanj_sir', self.vanj_sir),
            ('vanj_dulj', self.vanj_dulj),
            ('unu_vis', self.unu_vis),
            ('unu_sir', self.unu_sir),
            ('unu_dulj', self.unu_dulj),
            ('ulaz_vis', self.ulaz_vis),
            ('ulaz_sir', self.ulaz_sir),
            ('ulaz_ori', self.ulaz_ori),
            ('vodic', self.vodic),
            ('autor_unosa', self.autor_unosa),
            ('foto', self.foto)
        ]

    def get_object_info(self):
        return {
            'id': self.id,
            'type': self.LOCTYPE,
            'name': self.locality_title or self.naziv or 'Bez naziva'
        }


class ODKEasyData(models.Model):
    anonymous_author = models.CharField(
        max_length=60, null=True, blank=True,
        verbose_name=u'Autor'
    )
    name = models.CharField(
        max_length=100, null=True, blank=True, verbose_name=u'Naziv')
    collection_timestamp = models.DateTimeField()
    device_id = models.CharField(max_length=50)
    observation_uuid = models.CharField(max_length=50)


class ODKEasyPoint(Locality):
    image = models.ImageField(
        upload_to='odk_easy_images', verbose_name=u'Fotografija')
    point = models.PointField()

    height = models.FloatField(blank=True, null=True)
    accuracy = models.FloatField(blank=True, null=True)

    odkdata = models.ForeignKey('ODKEasyData')

    objects = models.GeoManager()

    LOCTYPE = 'ODKEasy'

    @classmethod
    def filter_by_location(cls, *args, **kwargs):
        return Q(**{
            '{0}__point__within'.format(cls._meta.module_name): args[0]
        })

    def get_short_text(self):
        return self.short_text.rendered if self.short_text else None

    def get_main_text(self):
        return self.main_text.rendered if self.main_text else None

    def get_short_info(self):
        return {
            'id': self.id,
            'title': self.odkdata.name or 'Bez naziva',
            'images': [self.image.name],
            'short_text': self.get_short_text(),
            'author': self.odkdata.anonymous_author or 'Nije poznat',
            'coordinate': format_coordinate(self.point),
            'coordinate_raw': self.point.wkt,
            'created': format_datetime(self.created),
            'source': self.LOCTYPE
        }

    def get_detailed_info(self):
        return {
            'id': self.id,
            'title': self.locality_title or self.odkdata.name or 'Bez naziva',
            'images': [self.image.name],
            'main_text': self.get_main_text(),
            'author': self.odkdata.anonymous_author or 'Nije poznat',
            'coordinate': format_coordinate(self.point),
            'created': format_datetime(self.created),
            'source': self.LOCTYPE
        }

    def get_original_data(self):
        return [
            ('id', self.id),
            ('collection_timestamp', self.odkdata.collection_timestamp),
            ('device_id', self.odkdata.device_id),
            ('observation_uuid', self.odkdata.observation_uuid),
        ]

    def get_object_info(self):
        return {
            'id': self.id,
            'type': self.LOCTYPE,
            'name': self.locality_title or self.odkdata.name or 'Bez naziva'
        }
