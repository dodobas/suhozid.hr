from django.test import TestCase

from model_factories import ODKEasyPointF, ODKEasyDataF


class testODKEasyPointModel(TestCase):
    def test_get_short_text_short_text_exists(self):
        """
        get_short_text() should return short_text attribute
        enclosed in <p> tag if it exists.
        """
        testODKEasyPoint = ODKEasyPointF(**{'short_text': 'TEST'})
        self.assertEqual(testODKEasyPoint.get_short_text(), '<p>TEST</p>')

    def test_get_short_text_short_text_doesnt_exists(self):
        """
        get_short_text() should return None if short_text attribute
        does not exist.
        """
        testODKEasyPoint = ODKEasyPointF()
        self.assertEqual(testODKEasyPoint.get_short_text(), None)

    def test_get_main_text_main_text_exists(self):
        """
        get_main_text() should return rendered main_text if it
        exists.
        """
        testODKEasyPoint = ODKEasyPointF(**{'main_text': 'TEST'})
        self.assertEqual(
            testODKEasyPoint.get_main_text(), '<p>TEST</p>'
        )

    def test_get_main_text_main_text_doesnt_exists(self):
        """
        get_main_text() should return None if main_text
        doesn't exists.
        """
        testODKEasyPoint = ODKEasyPointF()
        self.assertEqual(testODKEasyPoint.get_main_text(), None)

    def test_get_short_info_title_odkdata_name_exists(self):
        """
        If odkdata.name exists it has priority over other values.
        """
        odkData = ODKEasyDataF(**{'name': 'TEST'})
        testODKEasyPoint = ODKEasyPointF(**{'odkdata': odkData})
        titleValue = testODKEasyPoint.get_short_info()['title']
        self.assertEqual(titleValue, testODKEasyPoint.odkdata.name)

    def test_get_short_info_title_no_odkdata_name(self):
        """
        If no apropriate title exists, return 'Bez naziva'.
        """
        odkData = ODKEasyDataF(**{'name': ''})
        testODKEasyPoint = ODKEasyPointF(**{'odkdata': odkData})
        titleValue = testODKEasyPoint.get_short_info()['title']
        self.assertEqual(titleValue, 'Bez naziva')

    def test_get_short_info_anon_author_exists(self):
        """
        If anon author exists return it.
        """
        odkData = ODKEasyDataF(**{'anonymous_author': 'TEST'})
        testODKEasyPoint = ODKEasyPointF(**{'odkdata': odkData})
        authorValue = testODKEasyPoint.get_short_info()['author']
        self.assertEqual(
            authorValue, testODKEasyPoint.odkdata.anonymous_author
        )

    def test_get_short_info_no_author_exists(self):
        """
        If no author exists, return 'Nije poznat'
        """
        testODKEasyPoint = ODKEasyPointF()
        authorValue = testODKEasyPoint.get_short_info()['author']
        self.assertEqual(authorValue, 'Nije poznat')

    def test_get_detailed_info_locality_title_exists(self):
        """
        If locality_title exists it has priority over other values.
        """
        testODKEasyPoint = ODKEasyPointF()
        titleValue = testODKEasyPoint.get_detailed_info()['title']
        self.assertEqual(titleValue, testODKEasyPoint.locality_title)

    def test_get_detailed_info_odkdata_name_exists(self):
        """
        If no locality_title exists, but odkdata.name exists return it.
        """
        odkData = ODKEasyDataF(**{'name': 'TEST'})
        testODKEasyPoint = ODKEasyPointF(**{
            'locality_title': '',
            'odkdata': odkData
        })
        titleValue = testODKEasyPoint.get_detailed_info()['title']
        self.assertEqual(titleValue, testODKEasyPoint.odkdata.name)

    def test_get_detailed_info_no_title_value(self):
        """
        If no apropriate title exists, return 'Bez naziva'.
        """
        testODKEasyPoint = ODKEasyPointF(**{'locality_title': ''})
        titleValue = testODKEasyPoint.get_detailed_info()['title']
        self.assertEqual(titleValue, 'Bez naziva')

    def test_get_detailed_info_anon_author_exists(self):
        """
        If anon author exists return it.
        """
        odkData = ODKEasyDataF(**{'anonymous_author': 'TEST'})
        testODKEasyPoint = ODKEasyPointF(**{'odkdata': odkData})
        authorValue = testODKEasyPoint.get_detailed_info()['author']
        self.assertEqual(
            authorValue, testODKEasyPoint.odkdata.anonymous_author
        )

    def test_get_detailed_info_no_author_exists(self):
        """
        If no author exists, return 'Nije poznat'
        """
        testODKEasyPoint = ODKEasyPointF()
        authorValue = testODKEasyPoint.get_detailed_info()['author']
        self.assertEqual(authorValue, 'Nije poznat')

    def test_get_original_data(self):
        """
        Simple test for ID.
        """
        testODKEasyPoint = ODKEasyPointF()
        IDValue = testODKEasyPoint.get_original_data()[0][1]
        self.assertEqual(IDValue, testODKEasyPoint.id)

    def test_get_object_info_locality_title_exists(self):
        """
        If locality_title exists it has priority over other values.
        """
        testODKEasyPoint = ODKEasyPointF()
        nameValue = testODKEasyPoint.get_object_info()['name']
        self.assertEqual(nameValue, testODKEasyPoint.locality_title)

    def test_get_object_info_odkdata_name_exists(self):
        """
        If no locality_title exists, but odkdata.name exists return it.
        """
        odkData = ODKEasyDataF(**{'name': 'TEST'})
        testODKEasyPoint = ODKEasyPointF(**{
            'locality_title': '',
            'odkdata': odkData}
        )
        nameValue = testODKEasyPoint.get_object_info()['name']
        self.assertEqual(nameValue, testODKEasyPoint.odkdata.name)

    def test_get_object_info_no_name_exists(self):
        """
        If no apropriate name exists, return 'Bez naziva'.
        """
        testODKEasyPoint = ODKEasyPointF(**{'locality_title': ''})
        nameValue = testODKEasyPoint.get_object_info()['name']
        self.assertEqual(nameValue, 'Bez naziva')
