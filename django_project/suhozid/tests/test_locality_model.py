from django.test import TestCase

from core.model_factories import UserF
from model_factories import LocalityF


class TestLocalityModel(TestCase):
    def test_get_absolute_url(self):
        tstLocality = LocalityF.create(**{
            'id': 10000
        })

        self.assertEqual(tstLocality.get_absolute_url(), '/detail/10000/')

    def test_get_author(self):

        tstAuthor = UserF.create(**{
            'username': 'testuser'
        })
        tstLocality = LocalityF.create(**{
            'author': tstAuthor
        })

        self.assertEqual(tstLocality.get_author(), 'testuser')

    def test_get_author_null(self):
        tstLocality = LocalityF.create(**{
            'author': None
        })

        self.assertEqual(tstLocality.get_author(), None)
