from django.test import TestCase

from model_factories import ODKFanaticPointF, ODKFanaticDataF


class testODKFanaticPointModel(TestCase):
    def test_get_short_text_short_text_exists(self):
        """
        get_short_text() should return short_text attribute
        enclosed in <p> tag if it exists.
        """
        testODKFanaticPoint = ODKFanaticPointF(**{'short_text': 'TEST'})
        self.assertEqual(testODKFanaticPoint.get_short_text(), '<p>TEST</p>')

    def test_get_short_text_short_text_doesnt_exists(self):
        """
        get_short_text() should return None if short_text attribute
        does not exist.
        """
        testODKFanaticPoint = ODKFanaticPointF()
        self.assertEqual(testODKFanaticPoint.get_short_text(), None)

    def test_get_short_info_odkdata_name_exists(self):
        """
        If odkdata.name exists it has priority over other values.
        """
        testODKFanaticPoint = ODKFanaticPointF()
        titleValue = testODKFanaticPoint.get_short_info()['title']
        self.assertEqual(titleValue, testODKFanaticPoint.odkdata.name)

    def test_get_short_info_no_odkdata_name(self):
        """
        If no apropriate title exists, return 'Bez naziva'.
        """
        odkData = ODKFanaticDataF(**{'name': ''})
        testODKFanaticPoint = ODKFanaticPointF(**{'odkdata': odkData})
        titleValue = testODKFanaticPoint.get_short_info()['title']
        self.assertEqual(titleValue, 'Bez naziva')

    def test_get_short_info_short_text_exists(self):
        """
        If short_text exists it has priority over other values.
        It should also be enclosed in <p> tag.
        """
        testODKFanaticPoint = ODKFanaticPointF(**{'short_text': 'TEST'})
        shortTextValue = testODKFanaticPoint.get_short_info()['short_text']
        self.assertEqual(shortTextValue, '<p>TEST</p>')

    def test_get_short_info_remark_exists(self):
        """
        If short_text doesn't exists, but remark does return it.
        """
        testODKFanaticPoint = ODKFanaticPointF()
        shortTextValue = testODKFanaticPoint.get_short_info()['short_text']
        self.assertEqual(shortTextValue, testODKFanaticPoint.odkdata.remark)

    def test_get_short_info_short_text_no_values(self):
        """
        In case of no values in short_text or remark,
        return an empty string.
        """
        odkData = ODKFanaticDataF(**{'remark': ''})
        testODKFanaticPoint = ODKFanaticPointF(**{'odkdata': odkData})
        shortTextValue = testODKFanaticPoint.get_short_info()['short_text']
        self.assertEqual(shortTextValue, '')

    def test_get_short_info_anon_author_exists(self):
        """
        If anon author exists return it.
        """
        testODKFanaticPoint = ODKFanaticPointF()
        authorValue = testODKFanaticPoint.get_short_info()['author']
        self.assertEqual(
            authorValue, testODKFanaticPoint.odkdata.anonymous_author
        )

    def test_get_short_info_no_author_exists(self):
        """
        If no author exists, return 'Nije poznat'
        """
        odkData = ODKFanaticDataF(**{'anonymous_author': ''})
        testODKFanaticPoint = ODKFanaticPointF(**{'odkdata': odkData})
        authorValue = testODKFanaticPoint.get_short_info()['author']
        self.assertEqual(authorValue, 'Nije poznat')

    def test_get_detailed_info_main_text_exists(self):
        """
        If main_text exists it has priority over other values.
        It should also be enclosed in <p> tag.
        """
        testODKFanaticPoint = ODKFanaticPointF(**{'main_text': 'TEST'})
        mainTextValue = testODKFanaticPoint.get_detailed_info()['main_text']
        self.assertEqual(mainTextValue, '<p>TEST</p>')

    def test_get_detailed_info_remark_exists(self):
        """
        If remark exists, but main_text doesn't, return remark
        as main_text.
        """
        testODKFanaticPoint = ODKFanaticPointF()
        mainTextValue = testODKFanaticPoint.get_detailed_info()['main_text']
        self.assertEqual(mainTextValue, testODKFanaticPoint.odkdata.remark)

    def test_get_detailed_info_main_text_no_values(self):
        """
        In case of no values in main_text or remark,
        return an empty string.
        """
        odkData = ODKFanaticDataF(**{'remark': ''})
        testODKFanaticPoint = ODKFanaticPointF(**{'odkdata': odkData})
        mainTextValue = testODKFanaticPoint.get_detailed_info()['main_text']
        self.assertEqual(mainTextValue, '')

    def test_get_detailed_info_anon_author_exists(self):
        """
        If anon author exists return it.
        """
        testODKFanaticPoint = ODKFanaticPointF()
        authorValue = testODKFanaticPoint.get_detailed_info()['author']
        self.assertEqual(
            authorValue, testODKFanaticPoint.odkdata.anonymous_author
        )

    def test_get_detailed_info_no_author_exists(self):
        """
        If no author exists, return 'Nije poznat'
        """
        odkData = ODKFanaticDataF(**{'anonymous_author': ''})
        testODKFanaticPoint = ODKFanaticPointF(**{'odkdata': odkData})
        authorValue = testODKFanaticPoint.get_detailed_info()['author']
        self.assertEqual(authorValue, 'Nije poznat')

    def test_get_detailed_info_locality_title_exists(self):
        """
        If locality_title exists it has priority over other values.
        """
        testODKFanaticPoint = ODKFanaticPointF()
        titleValue = testODKFanaticPoint.get_detailed_info()['title']
        self.assertEqual(titleValue, testODKFanaticPoint.locality_title)

    def test_get_detailed_info_odkdata_name_exists(self):
        """
        If no locality_title exists, but odkdata.name exists return it.
        """
        testODKFanaticPoint = ODKFanaticPointF(**{'locality_title': ''})
        titleValue = testODKFanaticPoint.get_detailed_info()['title']
        self.assertEqual(titleValue, testODKFanaticPoint.odkdata.name)

    def test_get_detailed_info_no_title_value(self):
        """
        If no apropriate title exists, return 'Bez naziva'.
        """
        odkData = ODKFanaticDataF(**{'name': ''})
        testODKFanaticPoint = ODKFanaticPointF(**{
            'odkdata': odkData,
            'locality_title': ''}
        )
        titleValue = testODKFanaticPoint.get_detailed_info()['title']
        self.assertEqual(titleValue, 'Bez naziva')

    def test_get_original_data(self):
        """
        Simple test for ID.
        """
        testODKFanaticPoint = ODKFanaticPointF()
        IDValue = testODKFanaticPoint.get_original_data()[0][1]
        self.assertEqual(IDValue, testODKFanaticPoint.id)

    def test_get_object_info_locality_title_exists(self):
        """
        If locality_title exists it has priority over other values.
        """
        testODKFanaticPoint = ODKFanaticPointF()
        nameValue = testODKFanaticPoint.get_object_info()['name']
        self.assertEqual(nameValue, testODKFanaticPoint.locality_title)

    def test_get_object_info_odkdata_name_exists(self):
        """
        If no locality_title exists, but odkdata.name exists return it.
        """
        testODKFanaticPoint = ODKFanaticPointF(**{'locality_title': ''})
        nameValue = testODKFanaticPoint.get_object_info()['name']
        self.assertEqual(nameValue, testODKFanaticPoint.odkdata.name)

    def test_get_object_info_no_name_exists(self):
        """
        If no apropriate name exists, return 'Bez naziva'.
        """
        odkData = ODKFanaticDataF(**{'name': ''})
        testODKFanaticPoint = ODKFanaticPointF(**{
            'locality_title': '',
            'odkdata': odkData}
        )
        nameValue = testODKFanaticPoint.get_object_info()['name']
        self.assertEqual(nameValue, 'Bez naziva')
