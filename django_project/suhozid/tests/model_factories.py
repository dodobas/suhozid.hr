import datetime
import factory

from factory import fuzzy

from django.utils import timezone

from ..models import (
    Locality,
    WebPoint,
    ODKFanaticPoint,
    ODKFanaticData,
    Suhozid2013Fanatic,
    ODKEasyData,
    ODKEasyPoint
)


class LocalityF(factory.django.DjangoModelFactory):
    """
    OrderStatus model factory
    """
    FACTORY_FOR = Locality

    locality_title = factory.Sequence(lambda n: "LOC Title {0}".format(n))
    author = factory.SubFactory('core.model_factories.UserF')
    short_text = ''
    main_text = ''
    status = 2


class WebPointF(LocalityF):
    FACTORY_FOR = WebPoint

    anonymous_author = ''
    description = ''
    image_name = factory.Sequence(lambda num: 'image_{0}.jpg'.format(num))
    image = factory.django.ImageField(
        from_path='suhozid/tests/media/pic.jpg'
    )
    point = 'POINT (0 0)'
    collection_timestamp = datetime.datetime(2012, 1, 2, 1, 2, 3)


class ODKFanaticDataF(factory.django.DjangoModelFactory):
    FACTORY_FOR = ODKFanaticData

    anonymous_author = factory.Sequence(lambda n: "Author_%s" % n)
    localitytype = 2
    name = factory.Sequence(lambda n: "Naziv_%s" % n)
    remark = factory.Sequence(lambda n: "Napomena_%s" % n)
    outer_width = None
    outer_length = None
    outer_height = None
    inner_width = None
    inner_length = None
    inner_height = None
    entrance_height = None
    entrance_width = None
    entrance_orientation = None
    guide = None
    extra_photos = None
    collection_timestamp = (
        timezone.datetime(2013, 1, 2, 5, 2, 3).
        replace(tzinfo=timezone.utc)
    )
    device_id = fuzzy.FuzzyText(length=40)
    observation_uuid = fuzzy.FuzzyText(length=40)


class ODKFanaticPointF(LocalityF):
    FACTORY_FOR = ODKFanaticPoint

    image = factory.django.ImageField(
        from_path='suhozid/tests/media/pic.jpg'
    )
    point = 'POINT (0 0)'
    height = None
    accuracy = None
    odkdata = factory.SubFactory(ODKFanaticDataF)


class Suhozid2013FanaticF(LocalityF):
    FACTORY_FOR = Suhozid2013Fanatic

    wkb_geometry = 'POINT (0 0)'
    naziv = ""
    vrsta_lok = None
    opis_nap = ""
    vanj_vis = ""
    vanj_sir = ""
    vanj_dulj = ""
    unu_vis = ""
    unu_sir = ""
    unu_dulj = ""
    ulaz_vis = ""
    ulaz_sir = ""
    ulaz_ori = None
    vodic = ""
    autor_unosa = ""
    foto = ""


class ODKEasyDataF(factory.django.DjangoModelFactory):
    FACTORY_FOR = ODKEasyData

    anonymous_author = None
    name = None
    collection_timestamp = (
        timezone.datetime(2013, 1, 2, 5, 2, 3).
        replace(tzinfo=timezone.utc)
    )
    device_id = fuzzy.FuzzyText(length=40)
    observation_uuid = fuzzy.FuzzyText(length=40)


class ODKEasyPointF(LocalityF):
    FACTORY_FOR = ODKEasyPoint

    image = factory.django.ImageField(
        from_path='suhozid/tests/media/pic.jpg'
    )
    point = 'POINT (0 0)'
    height = None
    accuracy = None
    odkdata = factory.SubFactory(ODKEasyDataF)
