from django.test import TestCase

from model_factories import Suhozid2013FanaticF


class TestSuhozid2013FanaticModel(TestCase):
    def test_get_short_text_short_text_exists(self):
        """
        get_short_text() should return short_text attribute
        enclosed in <p> tag if it exists.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'short_text': 'TEST'})
        self.assertEqual(TestSuhozid.get_short_text(), '<p>TEST</p>')

    def test_get_short_text_short_text_doesnt_exists(self):
        """
        get_short_text() should return None if short_text attribute
        does not exist.
        """
        TestSuhozid = Suhozid2013FanaticF()
        self.assertEqual(TestSuhozid.get_short_text(), None)

    def test_prepare_images_foto_exists(self):
        """
        prepare_images() should return list of image name in format:
        [fotke/IMG_NAME]. IMG_NAME is splited by **.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'foto': 'TEST'})
        expectedResult = ['fotke/' + TestSuhozid.foto]
        self.assertEqual(TestSuhozid.prepare_images(), expectedResult)

    def test_prepare_images_foto_doesnt_exists(self):
        """
        If foto doesn't exist, return None.
        """
        TestSuhozid = Suhozid2013FanaticF()
        self.assertEqual(TestSuhozid.prepare_images(), None)

    def test_get_short_info_locality_title_exists(self):
        """
        If nazive exists return it.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'naziv': 'TEST'})
        titleValue = TestSuhozid.get_short_info()['title']
        self.assertEqual(titleValue, TestSuhozid.naziv)

    def test_get_short_info_no_title_value(self):
        """
        If no apropriate title exists, return 'Bez naziva'.
        """
        TestSuhozid = Suhozid2013FanaticF()
        titleValue = TestSuhozid.get_short_info()['title']
        self.assertEqual(titleValue, 'Bez naziva')

    def test_get_short_info_short_text_exists(self):
        """
        If short_text exists it has priority over other values.
        It should also be enclosed in <p> tag.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'short_text': 'TEST'})
        shortTextValue = TestSuhozid.get_short_info()['short_text']
        self.assertEqual(shortTextValue, '<p>TEST</p>')

    def test_get_short_info_opis_nap_exists(self):
        """
        If short_text doesn't exists and opis_nap exists, return description
        """
        TestSuhozid = Suhozid2013FanaticF(**{'opis_nap': 'TEST'})
        shortTextValue = TestSuhozid.get_short_info()['short_text']
        self.assertEqual(shortTextValue, TestSuhozid.opis_nap)

    def test_get_short_info_short_text_no_values(self):
        """
        In case of no values in short_text or opis_nap,
        return an empty string.
        """
        TestSuhozid = Suhozid2013FanaticF()
        shortTextValue = TestSuhozid.get_short_info()['short_text']
        self.assertEqual(shortTextValue, '')

    def test_get_short_info_author_exists(self):
        """
        If author exists return it.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'autor_unosa': 'TEST'})
        authorValue = TestSuhozid.get_short_info()['author']
        self.assertEqual(authorValue, TestSuhozid.autor_unosa)

    def test_get_short_info_no_author_exists(self):
        """
        If no author exists, return 'Nije poznat'
        """
        TestSuhozid = Suhozid2013FanaticF()
        authorValue = TestSuhozid.get_short_info()['author']
        self.assertEqual(authorValue, 'Nije poznat')

    def test_get_detailed_info_locality_title_exists(self):
        """
        If locality_title exists it has priority over other values.
        """
        TestSuhozid = Suhozid2013FanaticF()
        titleValue = TestSuhozid.get_detailed_info()['title']
        self.assertEqual(titleValue, TestSuhozid.locality_title)

    def test_get_detailed_info_naziv_exists(self):
        """
        If no locality_title exists, but naziv exists return it.
        """
        TestSuhozid = Suhozid2013FanaticF(**{
            'locality_title': '',
            'naziv': 'TEST'}
        )
        titleValue = TestSuhozid.get_detailed_info()['title']
        self.assertEqual(titleValue, TestSuhozid.naziv)

    def test_get_detailed_info_no_title_value(self):
        """
        If no apropriate title exists, return 'Bez naziva'.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'locality_title': ''})
        titleValue = TestSuhozid.get_detailed_info()['title']
        self.assertEqual(titleValue, 'Bez naziva')

    def test_get_detailed_info_main_text_exists(self):
        """
        If main_text exists it has priority over other values.
        It should also be enclosed in <p> tag.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'main_text': 'TEST'})
        mainTextValue = TestSuhozid.get_detailed_info()['main_text']
        self.assertEqual(mainTextValue, '<p>TEST</p>')

    def test_get_detailed_info_opis_nap_exists(self):
        """
        If opis_nap exists, but main_text doesn't, return opis_nap
        as main_text.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'opis_nap': 'TEST'})
        mainTextValue = TestSuhozid.get_detailed_info()['main_text']
        self.assertEqual(mainTextValue, TestSuhozid.opis_nap)

    def test_get_detailed_info_main_text_no_values(self):
        """
        In case of no values in main_text or opis_nap,
        return an empty string.
        """
        TestSuhozid = Suhozid2013FanaticF()
        mainTextValue = TestSuhozid.get_detailed_info()['main_text']
        self.assertEqual(mainTextValue, '')

    def test_get_detailed_info_author_exists(self):
        """
        If author exists it has priority over other values.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'autor_unosa': 'TEST'})
        authorValue = TestSuhozid.get_detailed_info()['author']
        self.assertEqual(authorValue, TestSuhozid.autor_unosa)

    def test_get_detailed_info_no_author_exists(self):
        """
        If no apropriate author exists, return 'Nije poznat'
        """
        TestSuhozid = Suhozid2013FanaticF()
        authorValue = TestSuhozid.get_detailed_info()['author']
        self.assertEqual(authorValue, 'Nije poznat')

    def test_get_original_data(self):
        """
        Simple test for ID.
        """
        TestSuhozid = Suhozid2013FanaticF()
        IDValue = TestSuhozid.get_original_data()[0][1]
        self.assertEqual(IDValue, TestSuhozid.id)

    def test_get_object_info_locality_title_exists(self):
        """
        If locality_title exists it has priority over other values.
        """
        TestSuhozid = Suhozid2013FanaticF()
        nameValue = TestSuhozid.get_object_info()['name']
        self.assertEqual(nameValue, TestSuhozid.locality_title)

    def test_get_object_info_image_name_exists(self):
        """
        If no locality_title exists, but naziv exists return it.
        """
        TestSuhozid = Suhozid2013FanaticF(**{
            'locality_title': '',
            'naziv': 'TEST'}
        )
        nameValue = TestSuhozid.get_object_info()['name']
        self.assertEqual(nameValue, TestSuhozid.naziv)

    def test_get_object_info_no_name_exists(self):
        """
        If no apropriate name exists, return 'Bez naziva'.
        """
        TestSuhozid = Suhozid2013FanaticF(**{'locality_title': ''})
        nameValue = TestSuhozid.get_object_info()['name']
        self.assertEqual(nameValue, 'Bez naziva')
