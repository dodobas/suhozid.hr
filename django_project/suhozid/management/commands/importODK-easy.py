from optparse import make_option

import dateutil.parser
import uuid

from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
import django.core.files

from core.utils import DictReader
from suhozid.models import ODKEasyData, ODKEasyPoint


class Command(BaseCommand):
    help = 'ImportODK sites'

    def _check_imported(self, UUID):
        actualData = ODKEasyData.objects.filter(
            observation_uuid=UUID).exists()
        if actualData:
            return True
        else:
            return False

    def handle(self, *args, **options):
        with open('/tmp/Suhozid_15032013_easy/data.csv', 'rb') as csvfile:

            myReader = DictReader(csvfile)
            for row in myReader:

                # try to read UUID data
                myUUID = row.get('observation_uuid')
                if myUUID:
                    if self._check_imported(myUUID):
                        # UUID already exists, continue to the next item
                        # (skip the rest of the method)
                        continue

                if not myUUID:
                    # generate UUID if needed, in case some devices fail to
                    # produce an UUID
                    myUUID = uuid.uuid4()

                myODK = ODKEasyData()
                myODK.observation_uuid = myUUID

                myODK.anonymous_author = row.get('author')
                myODK.collection_timestamp = dateutil.parser.parse(
                    row.get('trenutak_prikupljanja'))

                myODK.created = myODK.collection_timestamp

                myODK.device_id = row.get('user_id')

                myODK.save()
                myPoints = (
                    dict(
                        (attr.split('||') for attr in tocka.split('*|*')))
                    for tocka in row.get('repeat_observation').split('*$*'))

                for point in myPoints:
                    myODKPoint = ODKEasyPoint()
                    myCoords = map(float, point.get('lokacija').split(' '))

                    myODKPoint.point = Point(myCoords[1], myCoords[0])
                    myODKPoint.height = myCoords[2]
                    myODKPoint.accuracy = myCoords[3]

                    myODKPoint.odkdata = myODK

                    with open(
                        '/tmp/Suhozid_15032013_easy/media/{0}'.format(
                            point.get('photo')), 'rb') as myImage:
                        myODKPoint.image.save(
                            point.get('photo'),
                            django.core.files.File(myImage))
                    myODKPoint.save()
