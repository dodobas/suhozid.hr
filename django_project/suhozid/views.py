from django.views.generic import CreateView, DetailView, UpdateView
from django.views.generic.base import TemplateView, View
from django.http import HttpResponse
from django.views.defaults import page_not_found
from django.contrib.auth.models import User
from django.shortcuts import redirect

from braces.views import (
    LoginRequiredMixin, AjaxResponseMixin, JSONResponseMixin
)
import reversion
from reversion.helpers import generate_patch_html

from .forms import (
    WebPointForm,
    WebPointFormAnonymous,
    LocalityEditForm
)
from .models import WebPoint, Locality
from .utils import calculatePointBuffer, format_datetime


class ValidateFormMixin(object):
    def form_invalid(self, form):
        """
        If the form is invalid, re-render the context data with the
        data-filled form and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form), status=262)

    def form_valid(self, form):
        """
        If the form is valid, redirect to the supplied URL.
        """
        myObject = form.save(commit=False)
        # check if which kind of form we GOT
        if form.__class__.__name__ == 'WebPointForm':
            # associate user with a web point
            myObject.author = self.request.user
        self.object = myObject.save()

        return HttpResponse('OK')

    def get_form_class(self):
        """
        Returns the form class to use in this view.
        """
        if self.request.user.is_anonymous():
            return WebPointFormAnonymous
        else:
            return WebPointForm


class CreateWebPoint(ValidateFormMixin, CreateView):
    model = WebPoint
    template_name = 'create-form.html'


class MyDryWallPortalView(LoginRequiredMixin, TemplateView):
    template_name = 'mydrywallportal.html'

    def get_context_data(self, **kwargs):
        context = super(MyDryWallPortalView, self).get_context_data(**kwargs)
        context['personal_points'] = (
            locality.get_object_info()
            for locality in Locality.objects.filter(
                author=self.request.user).select_subclasses().all()
        )

        #get 25 localities to review. 'Status=2' => items in review. See model.
        context['points_to_review'] = (
            locality.get_object_info()
            for locality in Locality.objects.filter(
                status=2).select_subclasses().all()[:25]
        )

        #get number of localities
        context['number_of_points'] = (
            Locality.objects.select_subclasses().count()
        )

        #get number of localities owned by user
        context['number_of_claimed_points'] = (
            Locality.objects.filter(author=self.request.user)
            .select_subclasses().count()
        )
        return context


class GetFeatureInfo(AjaxResponseMixin, JSONResponseMixin, View):

    def get_ajax(self, request, *args, **kwargs):
        bbox = map(float, self.request.GET.get('bbox').split(','))
        height = int(self.request.GET.get('height'))
        width = int(self.request.GET.get('width'))
        clickX = int(self.request.GET.get('clickX'))
        clickY = int(self.request.GET.get('clickY'))

        myPoint = calculatePointBuffer(bbox, height, width, clickX, clickY)
        myObject = (
            obj.get_short_info() for obj in
            Locality.objects.filter_by_location(
                myPoint).select_subclasses()[:10]
        )

        return self.render_json_response(list(myObject))


class GetLocalityInfo(AjaxResponseMixin, JSONResponseMixin, View):

    def get_ajax(self, request, *args, **kwargs):
        myPK = int(self.request.GET.get('pk'))
        try:
            myObject = Locality.objects.filter(
                pk=myPK).select_subclasses().get()

            return self.render_json_response(myObject.get_short_info())
        except Locality.DoesNotExist:
            return page_not_found(request)


class LocalityDetailView(DetailView):
    template_name = 'locality_detail.html'
    queryset = Locality.objects.select_subclasses()

    def get_context_data(self, **kwargs):
        context = super(LocalityDetailView, self).get_context_data(**kwargs)

        context.update(self.object.get_detailed_info())

        return context


class LocalityEditView(LoginRequiredMixin, UpdateView):
    template_name = 'locality_edit.html'
    form_class = LocalityEditForm

    queryset = Locality.objects.select_subclasses()

    def get_form_kwargs(self):
        kwargs = super(LocalityEditView, self).get_form_kwargs()
        # substitute Locality object, as we use select_subclasses for original
        # queryset
        kwargs.update({
            'instance': Locality.objects.filter(pk=self.kwargs.get('pk')).get()
        })
        return kwargs

    def form_valid(self, form):
        with reversion.create_revision():
            self.object = form.save(commit=False)
            self.object.status = Locality.STATUS.in_review  # in review
            self.object.save()
            reversion.set_user(self.request.user)
        return super(LocalityEditView, self).form_valid(form)


class LocalityReviewView(LoginRequiredMixin, TemplateView):
    template_name = 'locality_review.html'

    def get_context_data(self, **kwargs):
        """
        Generates differences between 2 locality versions as context
        for template.
        """
        context = super(LocalityReviewView, self).get_context_data(**kwargs)

        #Get ID of object.
        pk = self.kwargs.get('pk')

        #Get object and use it to find current and previous
        #version of object.
        localityObject = Locality.objects.get(pk=pk)
        versionList = reversion.get_for_object(localityObject)
        newVersion = versionList[0]

        #In case there is only one version in database, use that version.
        #Even if it isn't really "old".
        try:
            oldVersion = versionList[1]
        except:
            oldVersion = versionList[0]

        #If user "owning" locality has changed, generate values to display
        #changes, if not, fill "unchangedAuthorUsername" with username of
        #unchanged author.
        #Also handle cases where user is None, in those cases use right
        #version of object and call get_short_info() to get author!
        newAuthorUsername = ''
        oldAuthorUsername = ''
        unchangedAuthorUsername = ''
        if newVersion.field_dict['author'] == oldVersion.field_dict['author']:
            try:
                unchangedAuthorID = oldVersion.field_dict['author']
                unchangedAuthor = User.objects.get(pk=unchangedAuthorID)
                unchangedAuthorUsername = unchangedAuthor.username
            #Handle oldAuthor = None & newAuthor = None, get author data from
            #get_short_info.
            except:
                instance = Locality.objects.select_subclasses().get(pk=pk)
                shortInfo = instance.get_short_info()
                unchangedAuthorUsername = shortInfo['author']

        else:
            try:
                newAuthorID = newVersion.field_dict['author']
                newAuthor = User.objects.get(pk=newAuthorID)
                newAuthorUsername = newAuthor.username
            #Handle oldAuthor = "name" & newAuthor = None, get newAuthor data
            #from get_short_info.
            except:
                instance = Locality.objects.select_subclasses().get(pk=pk)
                shortInfo = instance.get_short_info()
                newAuthorUsername = shortInfo['author']

            try:
                oldAuthorID = oldVersion.field_dict['author']
                oldAuthor = User.objects.get(pk=oldAuthorID)
                oldAuthorUsername = oldAuthor.username
            #Handle oldAuthor = None & newAuthor = "name", get oldAuthor data
            #from get_short_info by first reverting to old record, then
            #fetching data by calling get_short_info(), then revert to new
            #record!
            except:
                oldVersion.revert()
                instance = Locality.objects.select_subclasses().get(pk=pk)
                shortInfo = instance.get_short_info()
                oldAuthorUsername = shortInfo['author']
                newVersion.revert()

        #Generate HTML/CSS strings showing differences between version.
        localityTitleDiff = generate_patch_html(
            oldVersion, newVersion, 'locality_title', cleanup='semantic'
        )
        shortTextTitleDiff = generate_patch_html(
            oldVersion, newVersion, 'short_text', cleanup='semantic'
        )
        mainTextTitleDiff = generate_patch_html(
            oldVersion, newVersion, 'main_text', cleanup='semantic'
        )

        #Get user and date of last modification. Can be different from
        #currently owning the locality.
        userThatLastModifiedLocality = newVersion.revision.user
        dateCreated = format_datetime(newVersion.revision.date_created)

        #Fill context with data.
        context.update({
            'modificationUser': userThatLastModifiedLocality,
            'pk': pk,
            'dateCreated': dateCreated,
            'localityTitle': localityTitleDiff,
            'authorNew': newAuthorUsername,
            'authorOld': oldAuthorUsername,
            'authorSame': unchangedAuthorUsername,
            'shortText': shortTextTitleDiff,
            'mainText': mainTextTitleDiff}
        )

        return context

    def post(self, request, pk):
        """
        Use 'name' attribute of button elements to find out what action
        user wants to make.
        """
        instance = Locality.objects.get(pk=pk)
        versionList = reversion.get_for_object(instance)

        #If there is only one revision of Locality, clicking any of buttons
        #on review page, shouldn't do anything!
        if len(versionList) == 1:
            return redirect('localitydetail', pk=pk)

        if 'accept' in request.POST:
            #Set Locality object as "Published" and save it.
            instance.status = Locality.STATUS.published
            instance.save()
        elif 'decline' in request.POST:
            #Roll back to previous version of Locality object.
            #reversion.get_for_object returns a list of Versions,
            #zeroth element is the latest version of object.
            #First restore the old version back then save it to
            #create a new revision with "old" values.
            versionList[1].revert()

            with reversion.create_revision():
                versionList[1].object.save()
                reversion.set_user(request.user)

        return redirect('localitydetail', pk=pk)
