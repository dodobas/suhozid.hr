from datetime import datetime

from django.contrib.gis.geos.point import Point
from django.contrib.gis.geos.polygon import Polygon
from django.contrib.gis.gdal import SpatialReference, CoordTransform


WGS84 = SpatialReference(4326)
WORLDMERC = SpatialReference(900913)


def format_coordinate(point):
    """
    Format coordinate output

    Params:
        point - geos Point
    """
    if isinstance(point, Point):
        return u'lat: %.06f lon: %.06f' % point.coords
    else:
        return None


def calculatePointBuffer(bbox, height, width, clickX, clickY, pixel_offset=5):
    """
    Calculates rectangle polygon buffer 5px height/width
    """
    widthResolution = (bbox[2] - bbox[0]) / width
    heightResolution = (bbox[3] - bbox[1]) / height

    minx, maxx = (
        (clickX - pixel_offset) * widthResolution + bbox[0],
        (clickX + pixel_offset) * widthResolution + bbox[0])
    miny, maxy = (
        (height - clickY - pixel_offset) * heightResolution + bbox[1],
        (height - clickY + pixel_offset) * heightResolution + bbox[1])

    polygon = Polygon.from_bbox((minx, miny, maxx, maxy))
    # set polygon srid
    polygon.srid = 900913
    # transform it to WGS84 srs
    proj_transform = CoordTransform(WORLDMERC, WGS84)
    polygon.transform(proj_transform)

    return polygon


def format_datetime(datetime_instance):
    """
    Format datetime object to standard EU time.
    Returns string.

    :arg dt: datetime object to format
    :type value: datetime

    :returns: string representing standard EU time
    :rtype: string
    """

    if isinstance(datetime_instance, datetime):
        return datetime_instance.strftime("%H:%M:%S %d-%m-%Y")
    else:
        return None
