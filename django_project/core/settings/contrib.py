from .base import *

INSTALLED_APPS += [
    'south',
    'crispy_forms',
    'django_extensions',
    'pipeline',
    'social.apps.django_app.default',
    'reversion',
    'markitup',
]

PIPELINE_DISABLE_WRAPPER = True

PIPELINE_TEMPLATE_FUNC = '_.template'

STATICFILES_STORAGE = 'pipeline.storage.PipelineCachedStorage'


AUTHENTICATION_BACKENDS = (
    'social.backends.twitter.TwitterOAuth',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.google.GoogleOAuth2',
    'social.backends.persona.PersonaAuth',
)

SOCIAL_AUTH_FACEBOOK_KEY = '172650082893114'
SOCIAL_AUTH_FACEBOOK_SECRET = '7a4d65e3767a33db849a1d198c162219'

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '767110584213.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'np4JV1jQQu0y43M7o7MVHexR'

SOCIAL_AUTH_TWITTER_KEY = 'fFhw9ehusV2Iaqj4qCd1A'
SOCIAL_AUTH_TWITTER_SECRET = 'SUCmFIHcutEAYbOpGba23OgiwUoXdrb6Dk2ChQXeM'


MARKITUP_FILTER = ('markdown.markdown', {'safe_mode': True})
MARKITUP_SET = 'markitup/sets/markdown'
MARKITUP_SKIN = 'markitup/skins/markitup'

# set default crispyforms template pack
CRISPY_TEMPLATE_PACK = 'bootstrap'
