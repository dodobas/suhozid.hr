from .contrib import *

INSTALLED_APPS += [
    'web',
    'suhozid'
]

DATE_FORMAT = "d/m/Y H:i:s"

LOGIN_URL = '/prijava'
LOGIN_REDIRECT_URL = '/moj_suhozid'
LOGIN_ERROR_URL = '/prijava'

PIPELINE_JS = {
    'contrib': {
        'source_filenames': (
            'js/jquery-1.9.1.min.js',
            'js/jquery.form.js',
            'js/csrf-ajax.js',
            'js/underscore-min.js',
            'js/bootstrap.min.js',
            'js/jquery.routes.js',
            'js/jquery.mousewheel.js',
            'js/jquery.jscrollpane.min.js',
        ),
        'output_filename': 'js/contrib.js',
    },
    'core': {
        'source_filenames': (
            'js/init_project.js',
            'js/suhozid-modal.js',
            'js/create-edit-modal.js',
            'js/templates/*.jst'
        ),
        'output_filename': 'js/core.js',
    },
    'markitup': {
        'source_filenames': (
            'markitup/jquery.markitup.js',
            'markitup/sets/markdown/set.js',
        ),
        'output_filename': 'js/markitup.js'
    },
    'application': {
        'source_filenames': (
            'js/map.js',
            'js/app.js',
        ),
        'output_filename': 'js/application.js',
    },
    'social': {
        'source_filenames': (
            'js/social.js',
        ),
        'output_filename': 'js/social.js'
    }
}

PIPELINE_CSS = {
    'contrib': {
        'source_filenames': (
            'css/bootstrap.css',
            'css/bootstrap-responsive.css',
            'css/jquery.jscrollpane.css'
        ),
        'output_filename': 'css/contrib.css',
        'extra_context': {
            'media': 'screen, projection',
        },
    },
    'application': {
        'source_filenames': (
            'css/style.css',
        ),
        'output_filename': 'css/application.css',
        'extra_context': {
            'media': 'screen, projection',
        },
    },
    'markitup': {
        'source_filenames': (
            'markitup/skins/markitup/style.css',
            'markitup/sets/markdown/style.css',
        ),
        'output_filename': 'css/markitup.css',
        'extra_context': {
            'media': 'screen',
        },
    }
}
