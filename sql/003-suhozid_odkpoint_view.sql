-- create suhozid_web view

BEGIN;

CREATE VIEW suhozid_odkpoint_view AS
    SELECT ofp.locality_ptr_id, ofp.image, st_astext(ofp.point) as coord, ofp.point,
        ofd.anonymous_author, ofd.localitytype, ofd.remark, ofd.guide, ofd.name,
        ofd.extra_photos
    from suhozid_odkfanaticpoint ofp inner join suhozid_odkfanaticdata ofd ON ofp.odkdata_id=ofd.id;

GRANT SELECT, UPDATE ON suhozid_odkpoint_view TO normal_user;

CREATE RULE update_suhozid_odkpoint_view AS ON UPDATE TO suhozid_odkpoint_view
DO INSTEAD
UPDATE suhozid_odkfanaticpoint SET point = NEW.point
WHERE locality_ptr_id=OLD.locality_ptr_id;

CREATE RULE update_suhozid_odkpoint_view_data AS ON UPDATE TO suhozid_odkpoint_view
DO INSTEAD
UPDATE suhozid_odkfanaticdata ofd SET name = NEW.name, extra_photos=NEW.extra_photos, remark=NEW.remark,
    guide=NEW.guide, localitytype=NEW.localitytype
FROM suhozid_odkfanaticpoint ofp
WHERE ofd.id=ofp.odkdata_id AND ofp.locality_ptr_id=OLD.locality_ptr_id;
COMMIT;
