BEGIN;

CREATE TABLE "suhozid_odkeasydata" (
    "id" serial NOT NULL PRIMARY KEY,
    "anonymous_author" varchar(60),
    "name" varchar(100),
    "collection_timestamp" timestamp with time zone NOT NULL,
    "device_id" varchar(50) NOT NULL,
    "observation_uuid" varchar(50) NOT NULL
)
;
CREATE TABLE "suhozid_odkeasypoint" (
    "locality_ptr_id" integer NOT NULL PRIMARY KEY REFERENCES "suhozid_locality" ("id") DEFERRABLE INITIALLY DEFERRED,
    "image" varchar(100) NOT NULL,
    "height" double precision,
    "accuracy" double precision,
    "odkdata_id" integer NOT NULL REFERENCES "suhozid_odkeasydata" ("id") DEFERRABLE INITIALLY DEFERRED
)
;

SELECT AddGeometryColumn('suhozid_odkeasypoint', 'point', 4326, 'POINT', 2);
ALTER TABLE "suhozid_odkeasypoint" ALTER "point" SET NOT NULL;
CREATE INDEX "suhozid_odkeasypoint_point_id" ON "suhozid_odkeasypoint" USING GIST ( "point" GIST_GEOMETRY_OPS );
CREATE INDEX "suhozid_odkeasypoint_odkdata_id" ON "suhozid_odkeasypoint" ("odkdata_id");


COMMIT;
