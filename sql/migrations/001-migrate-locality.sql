BEGIN;
CREATE TABLE "suhozid_locality" (
    "id" serial NOT NULL PRIMARY KEY,
    "created" timestamp with time zone NOT NULL,
    "modified" timestamp with time zone NOT NULL,
    "locality_title" varchar(200) NOT NULL,
    "author_id" integer REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED,
    "short_text" text,
    "main_text" text,
    "status" integer NOT NULL,
    "_short_text_rendered" text NOT NULL,
    "_main_text_rendered" text NOT NULL
)
;



CREATE SEQUENCE tmp_seq;

-- webpoint

ALTER TABLE "suhozid_webpoint" ADD tmp_id INTEGER;
UPDATE "suhozid_webpoint" SET tmp_id = nextval('tmp_seq');
ALTER TABLE "suhozid_webpoint" RENAME id TO locality_ptr_id;
UPDATE "suhozid_webpoint" set locality_ptr_id = 10000 + locality_ptr_id;
UPDATE "suhozid_webpoint" set locality_ptr_id = tmp_id;
INSERT INTO suhozid_locality (
    id,created,modified, _short_text_rendered, _main_text_rendered, status, locality_title)
    SELECT locality_ptr_id,collection_timestamp,now(),'','',3,'' from suhozid_webpoint;

-- odkfanaticpoint

ALTER TABLE "suhozid_odkfanaticpoint" ADD tmp_id INTEGER;
UPDATE "suhozid_odkfanaticpoint" SET tmp_id =  nextval('tmp_seq');
ALTER TABLE "suhozid_odkfanaticpoint" RENAME id TO locality_ptr_id;
UPDATE "suhozid_odkfanaticpoint" set locality_ptr_id = 10000 + locality_ptr_id;
UPDATE "suhozid_odkfanaticpoint" set locality_ptr_id = tmp_id;
INSERT INTO suhozid_locality (
    id,created,modified, _short_text_rendered, _main_text_rendered, status, locality_title)
    SELECT locality_ptr_id,collection_timestamp,now(),'','',3,'' from suhozid_odkfanaticdata a inner join suhozid_odkfanaticpoint b on a.id = b.odkdata_id;


-- suhozid_2013_fanatic

ALTER TABLE "suhozid_2013_fanatic" ADD tmp_id INTEGER;
UPDATE "suhozid_2013_fanatic" SET tmp_id =  nextval('tmp_seq');
ALTER TABLE "suhozid_2013_fanatic" RENAME ogc_fid TO locality_ptr_id;
UPDATE "suhozid_2013_fanatic" set locality_ptr_id = 10000 + locality_ptr_id;
UPDATE "suhozid_2013_fanatic" set locality_ptr_id = tmp_id;
INSERT INTO suhozid_locality (
    id,created,modified, _short_text_rendered, _main_text_rendered, status, locality_title)
    SELECT locality_ptr_id,now()-'2 years'::interval,now(),'','',3,'' from suhozid_2013_fanatic;


-- clean

-- update the sequence
SELECT setval('suhozid_locality_id_seq', currval('tmp_seq'));

DROP SEQUENCE tmp_seq;


COMMIT;

BEGIN;
ALTER TABLE "suhozid_webpoint" DROP tmp_id;
ALTER TABLE "suhozid_webpoint" DROP author_id CASCADE;

ALTER TABLE "suhozid_odkfanaticpoint" DROP tmp_id;
ALTER TABLE "suhozid_2013_fanatic" DROP tmp_id;



ALTER TABLE "suhozid_webpoint" ADD
    CONSTRAINT "suhozid_webpoint_locality_ptr_id_fkey" FOREIGN KEY (locality_ptr_id) REFERENCES suhozid_locality(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "suhozid_odkfanaticpoint" ADD
    CONSTRAINT "suhozid_odkfanaticpoint_locality_ptr_id_fkey" FOREIGN KEY (locality_ptr_id) REFERENCES suhozid_locality(id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "suhozid_2013_fanatic" ADD
    CONSTRAINT "suhozid_2013_fanatic_locality_ptr_id_fkey" FOREIGN KEY (locality_ptr_id) REFERENCES suhozid_locality(id) DEFERRABLE INITIALLY DEFERRED;

-- clean south migration table
TRUNCATE south_migrationhistory;

-- create indexes
CREATE INDEX "suhozid_locality_author_id" ON "suhozid_locality" ("author_id");
COMMIT;
