-- create suhozid_web view

BEGIN;

CREATE VIEW suhozid_odkeasypoint_view AS
    SELECT oep.locality_ptr_id, oep.image, st_astext(oep.point) as coord, oep.point,
        oed.anonymous_author, oed.name
    from suhozid_odkeasypoint oep inner join suhozid_odkeasydata oed ON oep.odkdata_id=oed.id;

GRANT SELECT, UPDATE ON suhozid_odkpoint_view TO normal_user;

CREATE RULE update_suhozid_odkeasypoint_view AS ON UPDATE TO suhozid_odkpoint_view
DO INSTEAD
UPDATE suhozid_odkeasypoint SET point = NEW.point
WHERE locality_ptr_id=OLD.locality_ptr_id;

CREATE RULE update_suhozid_odkeasypoint_view_data AS ON UPDATE TO suhozid_odkpoint_view
DO INSTEAD
UPDATE suhozid_odkeasydata oed SET name = NEW.name
FROM suhozid_odkeasypoint oep
WHERE oed.id=oep.odkdata_id AND oep.locality_ptr_id=OLD.locality_ptr_id;
COMMIT;