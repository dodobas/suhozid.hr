-- create suhozid_2013_fanatic view

BEGIN;

CREATE VIEW suhozid_2013_fanatic_view AS
    SELECT *, st_astext(wkb_geometry) as coord
    from suhozid_2013_fanatic;

GRANT SELECT, UPDATE, DELETE, INSERT ON suhozid_2013_fanatic_view TO normal_user;
GRANT SELECT, UPDATE ON suhozid_2013_fanatic_ogc_fid_seq TO normal_user;

CREATE RULE update_suhozid_2013_fanatic_view AS ON UPDATE TO suhozid_2013_fanatic_view
DO INSTEAD
UPDATE suhozid_2013_fanatic SET
 wkb_geometry=NEW.wkb_geometry, naziv=NEW.naziv, vrsta_lok=NEW.vrsta_lok,
 opis_nap=NEW.opis_nap, vanj_vis=NEW.vanj_vis, vanj_sir=NEW.vanj_sir,
 vanj_dulj=NEW.vanj_dulj, unu_vis=NEW.unu_vis, unu_sir=NEW.unu_sir, unu_dulj=NEW.unu_dulj,
 ulaz_vis=NEW.ulaz_vis, ulaz_sir=NEW.ulaz_sir, ulaz_ori=NEW.ulaz_ori,
 vodic=NEW.vodic, autor_unosa=NEW.autor_unosa, foto=NEW.foto
WHERE locality_ptr_id=OLD.locality_ptr_id;

CREATE RULE insert_suhozid_odkpoint_view_data AS ON INSERT TO suhozid_2013_fanatic_view
DO INSTEAD
INSERT INTO suhozid_2013_fanatic (
 wkb_geometry, naziv, vrsta_lok, opis_nap, vanj_vis,
 vanj_sir, vanj_dulj, unu_vis, unu_sir, unu_dulj, ulaz_vis, ulaz_sir,
 ulaz_ori, vodic, autor_unosa, foto)
VALUES (NEW.wkb_geometry, NEW.naziv, NEW.vrsta_lok, NEW.opis_nap,
 NEW.vanj_vis, NEW.vanj_sir, NEW.vanj_dulj, NEW.unu_vis, NEW.unu_sir,
 NEW.unu_dulj, NEW.ulaz_vis, NEW.ulaz_sir, NEW.ulaz_ori, NEW.vodic, NEW.autor_unosa,
 NEW.foto);

CREATE RULE delete_suhozid_odkpoint_view_data AS ON DELETE TO suhozid_2013_fanatic_view
DO INSTEAD
DELETE FROM suhozid_2013_fanatic WHERE locality_ptr_id=OLD.locality_ptr_id;

COMMIT;
