#!/bin/bash
CWD=$(pwd)
MEDIAFILES=$CWD/django_project/mediafiles/


FILETYPES=('dodatne' 'fotke' 'odk_images' 'user_images' 'odk_easy_images')
FILESIZES=('240x180' '640x480')

cd $MEDIAFILES
for filetype in ${FILETYPES[@]}; do
    for filesize in ${FILESIZES[@]}; do

        PREFIX=${filesize%x*}
        # check if directory exists
        if [ ! -d "$PREFIX/$filetype" ]; then
            mkdir -p "$PREFIX/$filetype"
        fi

        FILELIST=($filetype/*)
        EXISTING_FILES=($PREFIX/$filetype/*)

        for file in "${FILELIST[@]}"; do
            if [[ ! ${EXISTING_FILES[@]} =~ "$PREFIX/$file" ]]; then
                echo "Converting to $PREFIX px: $file"
                convert -resize $filesize -background white -gravity center -extent $filesize -format jpg -quality 75 "$file" "$PREFIX/$file"
            fi
        done
    done
done
cd $CWD

